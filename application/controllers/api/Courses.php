<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Courses extends My_Controller { 

    public function __Construct() {
        parent::__construct();
        $this->load->model('courses_model');
    }


    // show all course table
    function show_all() {
        $this->notLoggedIn();

        $results = $this->courses_model->get_all_data();
        $data = array();
        $no = '0';

        foreach ($results as $result) {
            $no++;
            $row = array();
            $row['nos'] = $no;
            $row['id'] = $result->id;
            $row['course'] = $result->course;
            $row['description'] = $result->description;
            $row['date_created'] = $result->date_created;
            $row['date_modified'] = $result->date_modified;
            $data[] = $row;
        }

        $output = array(
            "courses" => $data,
            "recordsTotal" => $this->courses_model->count_all(),
        );

        echo json_encode($output);
    }


    // search course
    function search() {
        $this->notLoggedIn();

        $value = $this->input->post('text');
        $results = $this->courses_model->search_data($value);
        $data = array();
        $no = '0';
        
        foreach ($results as $result) {
            $no++;
            $row = array();
            $row['nos'] = $no;
            $row['id'] = $result->id;
            $row['course'] = $result->course;
            $row['description'] = $result->description;
            $row['date_created'] = $result->date_created;
            $row['date_modified'] = $result->date_modified;
            $data[] = $row;
        }

        $output = array(
            "courses" => $data,
            "recordsTotal" => $this->courses_model->count_all(),
        );

        echo json_encode($output);
    }

    // delete one course
    function delete_only($id) {
        $this->notLoggedIn();

        $result = $this->courses_model->delete_only_data($id);

        if($result){
            $msg['error'] = false;
            $msg['success'] = 'A course deleted successfully.';

            //activity
            $this->load->model('logs_model');
            $params = array(
                'user_id' => $this->session->userdata('user_id'),
                'action' => 'successfully DELETED a course.',
                'ip' =>  $_SERVER['REMOTE_ADDR'],
                'date_created' =>date("Y-m-d H:i:s"),
                'date_modified' =>date("Y-m-d H:i:s"),
            );
            $this->logs_model->add_log($params);
            /////////
        } else{
            $msg['error'] = true;
        }

        echo json_encode($msg);
    }

    // delete all course
    function delete_all() {
        $this->notLoggedIn();

        $result =  $this->courses_model->delete_all_data();

        if($result) {
            $msg['error'] = false;
            $msg['success'] = 'Course deleted successfully';

             //activity
             $this->load->model('logs_model');
             $params = array(
                 'user_id' => $this->session->userdata('user_id'),
                 'action' => 'successfully DELETED all course data.',
                 'ip' =>  $_SERVER['REMOTE_ADDR'],
                 'date_created' =>date("Y-m-d H:i:s"),
                 'date_modified' =>date("Y-m-d H:i:s"),
             );
             $this->logs_model->add_log($params);
             /////////
        } else {
            $msg['error'] = true;
        }
        
        echo json_encode($msg);
    }

    // add course
    function add() {
        $this->notLoggedIn();
        
        $config = array(
            // course field
            array('field' => 'course', 'label' => 'Course', 'rules' => 'trim|required'),
            // description
            array('field' => 'description', 'label' => 'Description', 'rules' => 'trim|required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $result['error'] = true;
            $result['msg'] = array(
                'course' => form_error('course'), 
                'description' => form_error('description'),
            );
        } else {
            $data = array(
                'course' => $this->input->post('course'),
                'description' => $this->input->post('description'),
                'date_created' =>date("Y-m-d H:i:s"),
            );

            // add new course
            if($this->courses_model->add_data($data)) {
                $result['error'] = false;
                $result['msg'] ='A new course added successfully.';

                //activity
                $this->load->model('logs_model');
                $params = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'action' => 'successfully ADDED a new course.',
                    'ip' =>  $_SERVER['REMOTE_ADDR'],
                    'date_created' =>date("Y-m-d H:i:s"),
                    'date_modified' =>date("Y-m-d H:i:s"),
                );
                $this->logs_model->add_log($params);
                /////////
            }
        }

        echo json_encode($result);
    }

    // update course
    function update() {
        $config = array(
            // course field
            array('field' => 'course', 
                  'label' => 'Course', 
                  'rules' => 'trim|required'),
            // description field
            array('field' => 'description',
                  'label' => 'Description', 
                  'rules' => 'trim|required'),
        );

        $this->form_validation->set_rules($config);

        if($this->form_validation->run() == FALSE) {
            $result['error'] = true;
            $result['msg'] = array(
                'course' => form_error('course'), 
                'description' => form_error('description'),
            );
        } else {
            $id = $this->input->post('id');
            $data = array(
                'course' => $this->input->post('course'),
                'description' => $this->input->post('description'),
                'date_modified' =>date("Y-m-d H:i:s"),
            );

            if($this->courses_model->update_data($id, $data)){
                $result['error'] = false;
                $result['success'] = 'Course updated successfully.';

                //activity
                $this->load->model('logs_model');
                $params = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'action' => 'successfully UPDATED a course.',
                    'ip' =>  $_SERVER['REMOTE_ADDR'],
                    'date_created' =>date("Y-m-d H:i:s"),
                    'date_modified' =>date("Y-m-d H:i:s"),
                );
                $this->logs_model->add_log($params);
                /////////
            }
        }

        echo json_encode($result);
    }
}