<?php defined('BASEPATH') OR exit('no direct scripts access allowed');

class Users extends My_Controller {

    public function __Construct() {
		parent::__construct();
        $this->load->model('user_model');
    }

    //login function
    public function login(){
        $validator = array('erro3r' => true, 'message' =>array());
        $validation_data = array(
        array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
        ),
              array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
        ));

        $this->form_validation->set_rules($validation_data);
        
        if($this->form_validation->run()===false){
          $validator['error'] = true;
            foreach($_POST as $key =>$value){
                $validator['message'][$key] = form_error($key);
            }
        }else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $login = $this->user_model->login($username, $password);
            if($login){
                $data = array('user_id' => $login,'logged_in' => true);
                $this->session->set_userdata($data);
                $validator['error'] = false;
                $validator['message']['success'] = 'dashboard';

                //activity
                $this->load->model('Logs_model');
                $params = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'action' => 'successfully login.',
                    'ip' =>  $_SERVER['REMOTE_ADDR'],
                    'date_created' =>date("Y-m-d H:i:s"),
                    'date_modified' =>date("Y-m-d H:i:s"),
                );
                $this->Logs_model->add_log($params);
                ///////////
            }else{
                $validator['error'] = true;
                $validator['message']['failed'] = 'INVALID USERNAME OR PASSWORD!'; 
            }
        }
        echo json_encode($validator);
    }

    // for logout function
    public function logout(){
        $this->notLoggedIn();

        //activity
        $this->load->model('Logs_model');
        $params = array(
            'user_id' => $this->session->userdata('user_id'),
            'action' => 'successfully logout.',
            'ip' =>  $_SERVER['REMOTE_ADDR'],
            'date_created' =>date("Y-m-d H:i:s"),
            'date_modified' =>date("Y-m-d H:i:s"),
        );
        $this->Logs_model->add_log($params);
        ///////////

        $this->session->sess_destroy();
        redirect('./');
    }

    // for table display
    function show_all_users() {
        $this->notLoggedIn();
        
        $results = $this->user_model->get_all_data();

        $data = array();

        $no = '0';

        foreach ($results as $result) {
            $no++;
            $row = array();
            $row['nos'] = $no;
            $row['userid'] = $result->id;
            $row['username'] = $result->username;
            $row['email'] = $result->email;
            $row['password'] = $result->password;
            $row['status'] = $result->status;
            $row['userlevel'] = $result->userlevel;
            $row['date_created'] = $result->datecreated;
            $row['date_modified'] = $result->datemodified;
            $row['rank'] = $result->rank;
            $row['firstname'] = $result->firstname;
            $row['middlename'] = $result->middlename;
            $row['lastname'] = $result->lastname;
            $row['suffixname'] = $result->suffixname;
            $row['afpsn'] = $result->afpsn;
            $row['bos'] = $result->bos;
            $row['afpos'] = $result->afpos;
            $row['address'] = $result->address;
            $row['phone'] = $result->phone;
            $row['photo'] = $result->photo;
            $data[] = $row;
        }

        $output = array(
            "users" => $data,
            "recordsTotal" => $this->user_model->count_all(),
        );

        echo json_encode($output) . "\r\n";
    }

    
    function add() {
        $this->notLoggedIn();

        $config = array(
            /* ***PERSONAL DETAILS*** */
            // RANK
            array('field' => 'rank', 'label' => 'Rank', 'rules' => 'trim|required'),
            // FIRSTNAME
            array('field' => 'firstname', 'label' => 'Firstname', 'rules' => 'trim|required'),
            // middlename or middle initial
            array('field' => 'middlename', 'label' => 'Middlename', 'rules' => 'trim|required'),
            // LASTNAME
            array('field' => 'lastname', 'label' => 'Lastname', 'rules' => 'trim|required'),
            // appendage
            //array('field' => 'appendage', 'label' => 'Appendage', 'rules' => 'trim|required'),
            // AFPSN
            array('field' => 'afpsn', 'label' => 'Afpsn', 'rules' => 'trim|required'),
            // BOS
            array('field' => 'bos', 'label' => 'Bos', 'rules' => 'trim|required'),
            // AFPOS
            array('field' => 'afpos', 'label' => 'Afpos', 'rules' => 'trim|required'),
            // ADDRESS
            array('field' => 'address', 'label' => 'Address', 'rules' => 'trim|required'),
            // CONTACT NUMBER
            array('field' => 'contact', 'label' => 'Contact', 'rules' => 'trim|required'),
            // EMAIL ADDRESS
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),

            /* ***ACCOUNT DETAILS*** */
            // USERNAME
            array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required'),
            // PASSWORD
            array('field' => 'password', 'label' => 'Password', 'rules' => 'trim|required'),
            // CONFIRM PASSWORD
            array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'trim|required|matches[password]'),
            // USER TYPE
            array('field' => 'usertype', 'label' => 'User Type', 'rules' => 'trim|required'),

            /* ***OTHER DETAILS*** */
            // PHOTO

        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $result['error'] = true;

            $result['msg'] = array(
                // PERSONAL INFORMATION
                'rank' => form_error('rank'), 
                'firstname' => form_error('firstname'), 
                'middlename' => form_error('middlename'), 
                'lastname' => form_error('lastname'), 
                //'appendage' => form_error('appendage'),
                'afpsn' => form_error('afpsn'),
                'bos' => form_error('bos'),
                'afpos' => form_error('afpos'),
                'address' => form_error('address'),
                'contact' => form_error('contact'),
                'email' => form_error('email'),

                // ACCOUNT DETAILS
                'username' => form_error('username'),
                'password' => form_error('password'),
                'confirm_password' => form_error('confirm_password'),
                'usertype' => form_error('usertype'),

                // OTHER DETAILS
            );
        } else {
            $data = array(
                 // PERSONAL INFORMATION
                 'rank' => $this->input->post('rank'), 
                 'firstname' => $this->input->post('firstname'), 
                 'middlename' => $this->input->post('middlename'), 
                 'lastname' => $this->input->post('lastname'), 
                 'suffixname' => $this->input->post('appendage'),
                 'afpsn' => $this->input->post('afpsn'),
                 'bos' => $this->input->post('bos'),
                 'afpos' => $this->input->post('afpos'),
                 'address' => $this->input->post('address'),
                 'phone' => $this->input->post('contact'),
                 
                 // ACCOUNT DETAILS
                 'username' => $this->input->post('username'),
                 'email' => $this->input->post('email'),
                 'password' => $this->input->post('password'),
                 'confirm_password' => $this->input->post('confirm_password'),
                 'status' => "Active",
                 'usertype' => $this->input->post('usertype'),

                 'date_created' => date("Y-m-d H:i:s"),
            );

            // add new user account
             if($this->user_model->add_data_1($data) && $this->user_model->add_data_2($data)) {
                $result['error'] = false;
                $result['msg'] ='A new user account added successfully.';

                //activity
                $this->load->model('logs_model');
                $params = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'action' => 'successfully ADDED a user account.',
                    'ip' =>  $_SERVER['REMOTE_ADDR'],
                    'date_created' =>date("Y-m-d H:i:s"),
                    //'date_modified' =>date("Y-m-d H:i:s"),
                );
                $this->logs_model->add_log($params);
                /////////
            }

        }

        echo json_encode($result);
    }

    // search function
    function search() {
        $this->notLoggedIn();

        $value = $this->input->post('text');
        $results = $this->user_model->search_data($value);
        $data = array();
        $no = '0';

        foreach ($results as $result) {
            $no++;
            $row = array();
            $row['nos'] = $no;
            $row['userid'] = $result->id;
            $row['username'] = $result->username;
            $row['email'] = $result->email;
            $row['password'] = $result->password;
            $row['status'] = $result->status;
            $row['userlevel'] = $result->userlevel;
            $row['date_created'] = $result->datecreated;
            $row['date_modified'] = $result->datemodified;
            $row['rank'] = $result->rank;
            $row['firstname'] = $result->firstname;
            $row['middlename'] = $result->middlename;
            $row['lastname'] = $result->lastname;
            $row['suffixname'] = $result->suffixname;
            $row['afpsn'] = $result->afpsn;
            $row['bos'] = $result->bos;
            $row['afpos'] = $result->afpos;
            $row['address'] = $result->address;
            $row['phone'] = $result->phone;
            $data[] = $row;
        }

        $output = array(
            "users" => $data,
            "recordsTotal" => $this->user_model->count_all(),
        );

        echo json_encode($output);
    }

    // delete one user
    function delete_only($id) {
        $this->notLoggedIn();

        $value = $this->input->post('text');
        $result = $this->user_model->search_data($value);
    }

    // delete all user
    function delete_all() {

    }

    // update user
    function update() {

    }
}