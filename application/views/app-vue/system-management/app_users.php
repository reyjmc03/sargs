<script type="text/javascript">
// tables and modal functions

import { upload }  from './file-upload.service';
const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;
var v = new Vue({
    el: '#app',
    data: {
        url: '<?php echo base_url(); ?>',
        addModal: false,
        editModal: false,
        deleteModal:false,
        users: [],
        search: { text: '' },
        emptyResult: false,
        searchQuery: null,
        newUser: {
            rank: '',
            firstname: '',
            middlename: '',
            lastname: '',
            appendage: '',
            afpsn: '',
            bos: '',
            afpos: '',
            contact: '',
            email: '',
            username: '',
            password: '',
            confirm_password: '',
            usertype: '',
        },
        chooseUser: {},
        formValidate: [],
        successMSG: '',

        //pagination
        currentPage:0,
        rowCountPage:8,
        totalRows:0,
        pageRange:3,
        sortBy: 'nos',

        currentUser: {},
        modalUser: {},



        // for select tag
        'choices': [1,2,3,4,5],

        
        select_rank: '',
        rank_data: '',

        select_bos: '',
        bos_data: '',

        select_afpos: '',
        afpos_data: '',
    },
    created() {
        this.showAll();
        this.fetchSelectTagRank();
        this.fetchSelectTagBOS();
        this.fetchSelectTagAFPOS();
    },
    computed: {
      isInitial() {
        return this.currentStatus === STATUS_INITIAL;
      },
      isSaving() {
        return this.currentStatus === STATUS_SAVING;
      },
      isSuccess() {
        return this.currentStatus === STATUS_SUCCESS;
      },
      isFailed() {
        return this.currentStatus === STATUS_FAILED;
      }
    },
    methods: {
        // image file upload
        onFileChange(e) {
            const file = e.target.files[0];
            this.url = URL.createObjectURL(file);
        },
        fetchSelectTagRank:function() {
            axios.get('<?php echo base_url(); ?>api/ranks/show_all').then(function(response){
                v.rank_data = response.data.ranks;
                //v.select_rank = '';
            })
        },
        fetchSelectTagBOS:function() {
            axios.get('<?php echo base_url(); ?>api/bos/show_all').then(function(response){
                v.bos_data = response.data.boss;
                //v.select_bos = '';
            })
        },
        fetchSelectTagAFPOS:function() {
            axios.get('<?php echo base_url(); ?>api/afpos/show_all').then(function(response){
                v.afpos_data = response.data.afposs;
                //v.select_afpos = '';
            })
        },
        //generate datatable using vuejs
        showAll(){
            axios.get('<?php echo base_url(); ?>api/users/show_all_users').then(function(response){
                if(response.data.users == null) {
                    v.noResult()
                } 
                else {
                    v.getData(response.data.users);
                    //console.log(response.data.users);
                }
            })
        },
        // to search a user
        searchUser() {
            var formData = v.formData(v.search);
            axios.post('<?php echo base_url(); ?>api/users/search', formData).then(function(response){
                if(response.data.users == null){
                    v.noResult()
                }else{
                    v.getData(response.data.users); 
                }  
            })
        },
        // add new user
        addUser() {
            var formData = v.formData(v.newUser);

            axios.post(this.url + "api/users/add", formData).then(function(response){
                if(response.data.error){
                    v.formValidate = response.data.msg;
                } else {
                    // confirmation
                    swal({
                        title:'Added!',
                        text: "A new user has been created.",
                        type: 'success',
                        icon: 'success', 
                    }).then((result) => {
                        $('#addModal').modal('hide');
                        v.clearAll();
                    });
                }
            })
        },
        // edit user
        editUser() {

        },
        formData(obj){
            var formData = new FormData();
            for(var key in obj) {
                formData.append(key, obj[key]);
            }
            return formData;
        },
        getData(users){
            v.emptyResult = false; // become false if has a record
            v.totalRows = users.length //get total of rows
            v.users = users.slice(v.currentPage * v.rowCountPage, (v.currentPage * v.rowCountPage) + v.rowCountPage); //slice the result for pagination
            
             // if the record is empty, go back a page
            if(v.users.length == 0 && v.currentPage > 0){ 
                v.pageUpdate(v.currentPage - 1)
                v.clearAll();  
            }
        },
        selectUser(user){
            v.chooseUser = user;
        },
        clearAll(){
            v.newUser = {};
            v.formValidate = false;
            v.addModal = false;
            v.editModal = false;
            v.deleteModal = false;
            v.refresh();
        },
        noResult(){
            v.emptyResult = true;  // become true if the record is empty, print 'No Record Found'
            v.users = null 
            v.totalUsers = 0 //remove current page if is empty
        },
        pageUpdate(pageNumber){
              v.currentPage = pageNumber; //receive currentPage number came from pagination template
                v.refresh()  
        },
        refresh(){
             v.search.text ? v.searchUser() : v.showAll(); //for preventing
        },
        showModal() {
            let element = this.$refs.modal.$el
            $(element).modal('show')
        },
        setCurrentUser: function(user) {
            this.currentUser = user
        },
        setUserMoreDetails: function(user) {
            this.modalUser = user
        },
        setChooseDetails: function(user) {
            this.modalUser = user
        },
        refresh() {
            v.search.text ? v.searchUser() : v.showAll(); // preventing
        },

        reset() {
            // reset form to initial state
            this.currentStatus = STATUS_INITIAL;
            this.uploadedFiles = [];
            this.uploadError = null;
        },
        save(formData) {
            // upload data to the server
            this.currentStatus = STATUS_SAVING;

            upload(formData)
            .then(x => {
                this.uploadedFiles = [].concat(x);
                this.currentStatus = STATUS_SUCCESS;
            })
            .catch(err => {
                this.uploadError = err.response;
                this.currentStatus = STATUS_FAILED;
            });
        },
        filesChange(fieldName, fileList) {
            // handle file changes
            const formData = new FormData();

            if (!fileList.length) return;

            // append the files to FormData
            Array
            .from(Array(fileList.length).keys())
            .map(x => {
                formData.append(fieldName, fileList[x], fileList[x].name);
            });

            // save it
            this.save(formData);
        }
    },
    mounted() {
        this.reset();
    },
})
</script>

