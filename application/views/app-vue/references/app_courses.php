<script type="text/javascript">
var v = new Vue({
    el: '#app',
    data: {
        url: '<?php echo base_url(); ?>',
        addModal: false,
        editModal: false,
        deleteModal:false,
        courses: [],
        search: { text: '' },
        emptyResult: false,
        searchQuery: null,
        newCourse: {
            course:'', 
            description:''
        },
        chooseCourse:{},
        formValidate:[],
        successMSG:'',

        //pagination
        currentPage:0,
        rowCountPage:8,
        totalRows:0,
        pageRange:3,
        sortBy: 'nos',

        currentCourse: {},
        modalCourse: {},
    },
    created() {
        this.showAll();
    },
    methods: {
        //generate datatable using vuejs
        showAll(){
            //axios.get('<?php //echo base_url(); ?>api/ranks/show_all').then(function(response){
            axios.get(this.url + "api/courses/show_all").then(function(response){
                if(response.data.courses == null) {
                    v.noResult()
                } 
                else {
                    v.getData(response.data.courses);
                }
            })
        },
        // to search a course
        searchCourse() {
            var formData = v.formData(v.search);
            axios.post(this.url + "api/courses/search", formData).then(function(response){
                if(response.data.courses == null){
                    v.noResult()
                }else{
                    v.getData(response.data.courses); 
                }  
            })
        },
        // add new course
        addCourse() {
           var formData = v.formData(v.newCourse);

            axios.post(this.url + "api/courses/add", formData).then(function(response){
                if(response.data.error){
                    v.formValidate = response.data.msg;
                } else {
                    // confirmation
                    swal({
                        title:'Added!',
                        text: "A new course has been created.",
                        type: 'success',
                        icon: 'success', 
                    }).then((result) => {
                        $('#addModal').modal('hide');
                        v.clearAll();
                    }); 
                }
            })
        },
         // update course
         updateCourse() {
            var formData = v.formData(v.chooseCourse); 
            swal({title: 'Are you sure?',
                //text: "Data is edit permanently!",
                icon:'warning', 
                buttons: true, 
                dangerMode: true
            }).then((willOUT) => {
                if (willOUT) {
                    axios.post(this.url + "api/courses/update", formData).then(function(response){
                        if(response.data.error){
                            v.formValidate = response.data.msg;
                        } else {
                             // confirmation
                            swal({
                                title:'Updated!',
                                text: "A course has been updated.",
                                type: 'success',
                                icon: 'success', 
                            }).then((result) => {
                                $('#editModal').modal('hide');
                                v.clearAll();
                            });
                        }
                    })
                }
            });
        }, 
        // to delete all courses
        deleteAll() {
            let inst = this;
            swal({title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon:'warning', 
                buttons: true, 
                dangerMode: true
            }).then((willOUT) => {
                if (willOUT) {
                    // confirmation
                    swal({
                        title:'Deleted!',
                        text: "All courses has been deleted.",
                        type: 'success',
                        icon: 'success', 
                    }).then((result) => {
                        axios.delete(this.url + "api/courses/delete_all")
                        v.clearAll();
                    });
                }
            });
        },
        // formdata
        formData(obj) {
            var formData = new FormData();
            for(var key in obj) {
                formData.append(key, obj[key]);
            }
            return formData;
        },
        // to delete one course
        deleteOne(id) {
            let inst = this;
            swal({title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon:'warning', 
                buttons: true, 
                dangerMode: true
            }).then((willOUT) => {
                if (willOUT) {
                    // confirmation
                    swal({
                        title:'Deleted!',
                        text: "Course has been deleted.",
                        type: 'success',
                        icon: 'success', 
                    }).then((result) => {
                        axios.delete(this.url + "api/courses/delete_only/" + id)
                        v.clearAll();
                    });
                }
            });
        },
        getData(courses){
            v.emptyResult = false; // become false if has a record
            v.totalRows = courses.length //get total of rows
            v.courses = courses.slice(v.currentPage * v.rowCountPage, (v.currentPage * v.rowCountPage) + v.rowCountPage); //slice the result for pagination
            
             // if the record is empty, go back a page
            if(v.courses.length == 0 && v.currentPage > 0){ 
                v.pageUpdate(v.currentPage - 1)
                v.clearAll();  
            }
        },
        selectRank(course){
            v.chooseRank = course;
        },
        clearMSG() {
            setTimeout(function() {
                v.successMSG = ''
            }, 3000); // disapprearing message success in 2 secs
        },
        clearAll(){
            v.newCourse = { course:'', description:'' };
            v.chooseCourse = { rank:'', description:'' };
            v.formValidate = false;
            v.addModal = false;
            v.editModal = false;
            v.deleteModal = false;
            v.refresh();
        },
        noResult(){
            v.emptyResult = true;  // become true if the record is empty, print 'No Record Found'
            v.courses = null 
            v.totalCourses = 0 //remove current page if is empty
        },
        pageUpdate(pageNumber){
              v.currentPage = pageNumber; //receive currentPage number came from pagination template
                v.refresh()  
        },
        showModal() {
            let element = this.$refs.modal.$el
            $(element).modal('show')
        },
        // for view details button
        setCurrentCourse: function(course) {
            this.currentCourse = course
        },
         // for edit
         setChooseCourse: function(course) {
            this.chooseCourse = course
        },
        setCourseMoreDetails: function(course) {
            this.modalCourse = course
        },
        refresh() {
            v.search.text ? v.searchCourse() : v.showAll(); // preventing
        },
    }
})
</script>