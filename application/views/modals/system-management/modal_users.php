<!-- CREATE NEW ACCOUNT MODAL -->
<div class="modal animate__animated animate__fadeIn" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1024px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-plus"></i>&nbsp;&nbsp;CREATE NEW USER ACCOUNT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- START -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- PERSONNAL DETAILS -->
                            <h5 class="card-title"><b><u>PERSONAL DETAILS</u></b></h5><br>
                            <div class="form-group">
                                <h6 class="control-label">RANK: </h6>
                                <select class="form-control input-lg" v-model="select_rank" @change="fetchSelectTagRank" name="rank" :class="{'is-invalid': formValidate.rank}" v-model="newUser.rank">
                                    <option value="">-- Select --</option>
                                    <option v-for="drank in rank_data" :value="drank.rank">{{ drank.rank }} - {{ drank.description }}</option>
                                </select>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.rank" v-html="formValidate.rank"></p>
                            </div><hr>
                            <div class="form-group">
                                <h6 class="control-label">NAME: </h6>
                                <input type="text" placeholder="Enter Firstname" class="form-control" name="firstname" :class="{'is-invalid': formValidate.firstname}" v-model="newUser.firstname">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.firstname" v-html="formValidate.firstname"></p>
                                <br>
                                <input type="text" placeholder="Enter Middlename" class="form-control" name="middlename" :class="{'is-invalid': formValidate.middlename}" v-model="newUser.middlename">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.middlename" v-html="formValidate.middlename"></p>
                                <br>
                                <input type="text" placeholder="Enter Lastname" class="form-control"  name="lastname" :class="{'is-invalid': formValidate.lastname}" v-model="newUser.lastname">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.lastname" v-html="formValidate.lastname"></p>
                                <br>
                                <input type="text" placeholder="Appendage (e.g. Sr., Jr., II or III)" class="form-control" name="appendage" :class="{'is-invalid': formValidate.appendage}" v-model="newUser.appendage">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.appendage" v-html="formValidate.appendage"></p>
                            </div><hr>
                            <div class="form-group">
                                <h6 class="control-label">AFPSN: </h6>
                                <input type="text" placeholder="Enter AFPSN" class="form-control"  name="afpsn" :class="{'is-invalid': formValidate.afpsn}" v-model="newUser.afpsn">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.afpsn" v-html="formValidate.afpsn"></p>
                                <br>
                                <h6 class="control-label">BOS: </h6>
                                <select class="form-control input-lg" v-model="select_bos" @change="fetchSelectTagBOS" name="bos" :class="{'is-invalid': formValidate.bos}" v-model="newUser.bos">
                                    <option value="">-- Select --</option>
                                    <option v-for="dbos in bos_data" :value="dbos.bos">{{ dbos.bos }} - {{ dbos.description }}</option>
                                </select>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.bos" v-html="formValidate.bos"></p>
                                <br>   
                                <h6 class="control-label">AFPOS: </h6>
                                <select class="form-control input-lg" v-model="select_afpos" @change="fetchSelectTagAFPOS" name="afpos" :class="{'is-invalid': formValidate.afpos}" v-model="newUser.afpos">
                                    <option value="">-- Select --</option>
                                    <option v-for="dafpos in afpos_data" :value="dafpos.afpos">{{ dafpos.afpos }} - {{ dafpos.description }}</option>
                                </select> 
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.afpos" v-html="formValidate.afpos"></p>                           
                            </div>
                            <div class="form-group">
                                <h6 class="control-label">ADRRESS: </h6>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"  name="address" :class="{'is-invalid': formValidate.address}" v-model="newUser.address">
                                </textarea>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.address" v-html="formValidate.address"></p>
                                <br>
                                <h6 class="control-label">CONTACT #: </h6>
                                <input type="text" placeholder="Enter Telephone or Mobile Number" class="form-control" name="contact" :class="{'is-invalid': formValidate.contact}" v-model="newUser.contact">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.contact" v-html="formValidate.contact"></p>
                                <br>
                                <h6 class="control-label">EMAIL ADDRESS: </h6>
                                <input type="text" placeholder="Enter Email Address" class="form-control" name="email" :class="{'is-invalid': formValidate.email}" v-model="newUser.email">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.email" v-html="formValidate.email"></p>
                                <br>
                            </div>
                        </div>
                       <!-- ACCOUNT DETAILS --> 
                        <div class="col-lg-6">
                            <h5 class="card-title"><b><u>ACCOUNT DETAILS</u></b></h5><br>
                            <div class="form-group">
                                <h6 class="control-label">USERNAME: </h6>
                                <input type="text" placeholder="Enter Username" class="form-control" name="username" :class="{'is-invalid': formValidate.username}" v-model="newUser.username">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.username" v-html="formValidate.username"></p>
                                <br>
                            </div><hr>
                            <div class="form-group">
                                <h6 class="control-label">PASSWORD: </h6>
                                <input type="text" placeholder="Enter Password" class="form-control" name="password" :class="{'is-invalid': formValidate.password}" v-model="newUser.password">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.password" v-html="formValidate.password"></p>
                                <br>
                                <input type="text" placeholder="Enter Confirm Password" class="form-control" name="confirm_password" :class="{'is-invalid': formValidate.confirm_password}" v-model="newUser.confirm_password">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.confirm_password" v-html="formValidate.confirm_password"></p>
                                <br>
                            </div><hr>
                            <div class="form-group">
                                <h6 class="control-label">USER TYPE: </h6>
                                <select class="form-control input-lg" name="usertype" :class="{'is-invalid': formValidate.usertype}" v-model="newUser.usertype">
                                    <option value="">-- Select --</option>
                                    <option value="Administrator">ADMINISTRATOR</option>
                                    <option value="User">USER</option>
                                    <option value="CourseNCO">COURSE NCO</option>
                                    <option value="CourseDirector">COURSE DIRECTOR</option>
                                </select>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.usertype" v-html="formValidate.usertype"></p>
                                <br>
                            </div><hr>
                            <h5 class="card-title"><b><u>OTHER DETAILS</u></b></h5><br>
                            <!-- <form class="form-group"> -->
                            <form class="form-group" enctype="multipart/form-data" novalidate v-if="isInitial || isSaving">
                                <h6 class="control-label">PHOTO OF USER: </h6>
                                <div class="dropbox">
                                    <input type="file" multiple :name="uploadFieldName" :disabled="isSaving" @change="filesChange($event.target.name, $event.target.files); fileCount = $event.target.files.length"
                                    accept="image/*" class="input-file">
                                    <p v-if="isInitial">
                                        Drag your file(s) here to begin<br> or click to browse
                                    </p>
                                    <p v-if="isSaving">
                                        Uploading {{ fileCount }} files...
                                    </p>
                                </div>
                            </form>



                            <!-- <div class="form-group">
                                <h6 class="control-label">PHOTO OF USER: </h6>
                                <input type="file" @change="onFileChange">
                                <div v-if="url">
                                    <center><img :src="url" class="img-thumbnail" width="250" height="250"></center>
                                </div> -->
                                <!-- <div class="dropbox">
                                    <input type="file" multiple="multiple" name="photos" accept="image/*" class="input-file"> 
                                    <p>Drag your file(s) here to begin<br> or click to browse</p>
                                </div> -->
                                <!-- <div v-else-if="!url">
                                    <img src="http://placehold.it/250x250" class="img-thumbnail" width="250" height="250">
                                    
                                </div>
                            </div> -->

                           
                            <div class="form-group modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times" ></i> Cancel</button>
                                <button class="btn btn-warning" @click="addUser"><i class="fas fa-plus"></i> Create</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END -->
            </div>
            <!-- <div class="modal-footer">
            </div> -->
        </div>
    </div>
</div>



<!-- MORE DETAILS MODAL -->
<div class="modal animate__animated animate__fadeIn" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1024px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-eye"></i>&nbsp;&nbsp;MORE DETAILS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5 class="card-title"><b><u>PERSONAL INFORMATION</u></b></h5>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Name of User:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                               
                                {{modalUser.id}}{{modalUser.rank}}&nbsp;{{modalUser.firstname}}&nbsp;{{modalUser.middlename}}&nbsp;{{modalUser.lastname}}{{modalUser.suffixname == null ? '' : '&nbsp;' + modalUser.suffixname}}{{modalUser.afpsn== null ? '' : '&nbsp;' + modalUser.afpsn}}{{modalUser.afpos== null ? '' : '&nbsp;' + modalUser.afpos}}{{modalUser.bos== null ? '' : '&nbsp;' + modalUser.bos}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Home Address:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.address}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Contact #:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.phone}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Photo:</b></h6>
                                <center><img :src="'uploads/system-management/' + modalUser.userid+ '.jpg'" class="img-thumbnail" alt="Cinque Terre" width="250" height="250"></center>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5 class="card-title"><b><u>ACCOUNT INFORMATION</u></b></h5>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Username:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.username}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Email Address:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.email}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>User Level:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                    <div v-if="modalUser.userlevel == 1">
                                        ADMININSTRATOR
                                    </div>
                                    <div v-else>
                                        COURSE NCO
                                    </div>
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Status:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.status}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Date/Time Created:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.date_created}}
                                </label>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><b>Date/Time Updated:</b></h6>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="control-label">
                                {{modalUser.date_modified}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-danger" data-toggle="modal"><i class="fas fa-plus"></i>&nbsp; ADD USER</button> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- EDIT MODAL -->
<div class="modal animate__animated animate__fadeIn" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1024px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-pen"></i>&nbsp;&nbsp;UPDATE USER ACCOUNT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-danger" data-toggle="modal"><i class="fas fa-plus"></i>&nbsp; ADD USER</button> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


