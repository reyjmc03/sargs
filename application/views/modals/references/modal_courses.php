<!-- add Course Modal -->
<modal class="modal animate__animated animate__fadeIn" tabindex="-1" role="dialog" aria-hidden="true" id="addModal" @close="clearAll()">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-plus"></i>&nbsp;Create (Course)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h6 class="control-label"><strong>COURSE:</strong></h6>
                                <input type="text" class="form-control" :class="{'is-invalid': formValidate.course}" name="course" v-model="newCourse.course">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.course" v-html="formValidate.course"></p>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><strong>DESCRIPTION:</strong></h6>
                                <textarea cols="35" rows="5" :class="{'is-invalid': formValidate.description}" name="description" v-model="newCourse.description" class="form-control"></textarea>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.description" v-html="formValidate.description"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times" ></i> Cancel</button>
                <button class="btn btn-warning" @click="addCourse"><i class="fas fa-plus"></i> Create</button>
            </div>
        </div>
    </div>
</modal>



<!-- edit course modal -->
<modal class="modal animate__animated animate__fadeIn" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-pen"></i>&nbsp;&nbsp;Edit (Course)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h6 class="control-label"><strong>COURSE:</strong></h6>
                                <input type="text" class="form-control" :class="{'is-invalid': formValidate.course}" name="course" v-model="chooseCourse.course">
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.course" v-html="formValidate.course"></p>
                            </div>
                            <div class="form-group">
                                <h6 class="control-label"><strong>DESCRIPTION:</strong></h6>
                                <textarea cols="35" rows="5" :class="{'is-invalid': formValidate.description}" name="description" v-model="chooseCourse.description" class="form-control">
                                {{chooseCourse.description}}
                                </textarea>
                                <p class="text-danger animate__animated animate__fadeIn" v-if="formValidate.description" v-html="formValidate.description"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>
                <button class="btn btn-success" @click="updateCourse"><i class="fas fa-pen"></i> Update</button>
            </div>
        </div>
    </div>
</modal>

<!-- detail Ranks Modal -->
<modal class="modal animate__animated animate__fadeIn" id="detailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-eye"></i>&nbsp;&nbsp;Details (Course)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="control-label"><strong>NUMBER ID:</strong></h6>
                            <label style="color:black;" class="form-control">{{currentCourse.nos}}</label><br>
                            <h6 class="control-label"><strong>COURSE:</strong></h6>
                            <label style="color:black;" class="form-control">{{currentCourse.course}}</label><br>
                            <h6 class="control-label"><strong>DESCRIPTION:</strong></h6>
                            <textarea style="color:black;" cols="35" rows="5" :class="{'is-invalid': formValidate.description}" name="description" v-model="currentCourse.description" class="form-control" disabled></textarea>
                        </div>
                        <div class="col-md-6">
                            <h6 class="control-label"><strong>ACTIVITIES:</strong></h6>
                            <label style="color:blue;" class="form-control">created:&nbsp;{{currentCourse.date_created}}</label>
                            <label style="color:red;" class="form-control">updated:&nbsp;{{currentCourse.date_modified}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</modal>

