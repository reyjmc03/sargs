<main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main">
          <h1 class="bd-title" id="content">STUDENT ADMISSION, REGISTRATION AND GRADING SYSTEM</h1>
          <p class="bd-lead">Links to community-translated Bootstrap documentation sites.</p>
          <script async src="https://cdn.carbonads.com/carbon.js?serve=CKYIKKJL&placement=getbootstrapcom" id="_carbonads_js"></script>

          <p>Community members have translated Bootstrap’s documentation into various languages. None are officially supported and they may not always be up to date.</p>

<ul>

  <li><a href="http://bootstrap.hexschool.com/" hreflang="zh-tw">Bootstrap 4 繁體中文手冊 (中文(繁體))</a></li>

  <li><a href="https://code.z01.com/v4" hreflang="zh">Bootstrap 4 · 全球最流行的 HTML、CSS 和 JS 工具库。 (Chinese)</a></li>

  <li><a href="http://getbootstrap.com.br/v4/" hreflang="pt-BR">Bootstrap 4 Português do Brasil (Brazilian Portuguese)</a></li>

</ul>

<p><strong>We don’t help organize or host translations, we just link to them.</strong></p>

<p>Finished a new or better translation? Open a pull request to add it to our list.</p>

        </main>