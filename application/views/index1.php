<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/titletag'); ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- app favicon -->
        <?php $this->load->view('includes/favicon'); ?>


         <!-- Bootstrap Css -->
         <link href="<?php echo base_url();?>assets/overlay/nazox/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
         
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/font-awesome.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/icon-font.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/animate.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/hamburgers.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/select2.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/util.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/overlay/Login_v12/main.css">


         <!-- Fontawesome CSS -->
         <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/pre-skool/assets/plugins/fontawesome/css/fontawesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/pre-skool/assets/plugins/fontawesome/css/all.min.css">



        <style>
           .container-login100::before {
                /* background: red; */
                background: -webkit-linear-gradient(bottom, #d5dea7, green) !important;
           } 

           .login100-form-avatar {
               width: 350px !important;
               height: 350px !important;
           }

           .login100-form-sub-title {
                font-family: Montserrat-ExtraBold;
                font-size: 20px;
                line-height: 1.2;
                text-align: center;
                width: 100%;
                display: block;
            }

            body {
                zoom: 80%;
            }
        </style>

    </head>
    <body class="auth-body-bg">
        <div class="limiter">
            <div class="container-login100" style="background-image: url('assets/images/c2ctradoc.jpg');">
                <div class="wrap-login100 p-t-190 p-b-30">
                    <form class="login100-form validate-form">
                        <div class="login100-form-avatar">
                            <img src="<?php echo base_url(); ?>assets/images/C2C.png" alt="AVATAR">

                            <img src="<?php echo base_url(); ?>assets/images/C2C.png" alt="AVATAR">
                        </div>
                        
                        <span class="login100-form-title p-t-30">STUDENT ADMISSION, REGISTRATION AND GRADING SYSTEM</span>
                        <p class="login100-form-sub-title text-warning p-t-10 p-b-20">version 1.0.0</p>



                        



                        <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
                            <input class="input100" type="text" name="username" placeholder="Please Enter Username">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user"></i>
                            </span>
                            <p class="text-danger animate__animated animate__fadeIn" v-if="message.password" v-html="message.password">asdasdasdasdasd</p>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10" data-validate="Password is required">
                            <input class="input100" type="password" name="pass" placeholder="Please Enter Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock"></i>
                            </span>
                            <p class="text-danger animate__animated animate__fadeIn" v-if="message.password" v-html="message.password">asdasdasdasdasd</p>
                        </div>


                        

                        <div class="container-login100-form-btn p-t-10">
                            <input type="submit" class="login100-form-btn" value="Login" @click="login()">
                        </div>

                        <div class="text-center w-full p-t-25 p-b-230">
                            <!-- <a href="#" class="txt1"> Forgot Username / Password? </a> -->
                        </div>

                        <div class="text-center w-full">
                            <a class="txt1" href="#"> Create new account <i class="fa fa-long-arrow-right"></i>
                            </a>

                            
                        </div>
                    </form>

                </div>
            </div>
        </div>


       <!-- JAVASCRIPT -->
       <script> 
            var url = "<?php echo base_url(); ?>"
        </script>
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vue.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vuex.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/axios.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/login.js"></script>
        <script src="<?php echo base_url();?>assets/overlay/Login_v12/select2.min.js"></script>
    </body>
</html>