
<!DOCTYPE html>
<html  class="perfect-scrollbar-on" lang="en">
    <head>
        <meta charset="utf-8" />
        <?php $this->load->view('includes/titletag'); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
    
        <!-- app favicon -->
        <?php $this->load->view('includes/favicon'); ?>

        <!-- login css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/css/login.css" />

        <!-- Bootstrap Css -->
        <link href="<?php echo base_url();?>assets/overlay/nazox/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />

        <!-- Icons Css -->
        <link href="<?php echo base_url();?>assets/overlay/nazox/assets/css/icons.min.css" rel="stylesheet" type="text/css" />

        <!-- App Css-->
        <link href="<?php echo base_url();?>assets/overlay/nazox/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />


        <!-- animate css -->
        <link href="<?php echo base_url();?>assets/overlay/animatecss/animate.min.css" id="animate-style" rel="stylesheet" type="text/css" />

        <!-- ALTER STYLESHEET -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/css/alter.css">

        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/pre-skool/assets/plugins/fontawesome/css/fontawesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/pre-skool/assets/plugins/fontawesome/css/all.min.css">



        <!-- Main CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/overlay/pre-skool/assets/css/style.css">

        <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/overlay/css/poppins-font.css">

        <style>
        .auth-form-group-custom .auti-custom-input-icon {
           color: black !important;
        }

        body {
            zoom: 80% !important;
        }

        .auth-body-bg::before {
            background: -webkit-linear-gradient(bottom, #d5dea7, green) !important;
            content: "" !important;
            display: block !important;
            position: absolute !important;
            z-index: -1 !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            opacity: 0.9 !important;
        }
        .auth-body-bg {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items:center;
            padding: 15px;
            background-repeat: no-repeat !important;
            background-size: cover !important;
            background-position: center !important;
            position: relative;
            z-index: 1;
        }

        .loginpagetitle {
            color: #FFF1;
            font-size: 35px;
            text-shadow: 1px 1px #333;
        }

        
        </style>

    </head>
    <body class="auth-body-bg" style="background-image: url('assets/images/c2ctradoc.jpg') !important;">

        <main role="main" class="container p-0">
                <div class="row no-gutters">

                    <!-- login form -->
                    <div class="col-lg-12">
                        <div class="authentication-page-content p-4 d-flex align-items-center min-vh-100">
                            <div class="w-100">
                                <div class="row justify-content-center">
                                    <div>
                                        <div>
                                            <div class="text-center">
                                                <div class="animate__animated animate__fadeIn">
                                                    <a href="<?php echo base_url(); ?>" class="logo">
                                                        <img src="<?php echo base_url(); ?>assets/images/C2C.png" height="400" width="400" alt="logo">
                                                    </a>
                                                </div>
                                                <h1 class="font-size-18 mt-4"></h1>
                                                <h1 class="loginpagetitle"><strong>STUDENT ADMISSION, REGISTRATION</strong></h1>
                                                <h1 class="loginpagetitle"><strong>AND GRADING SYSTEM</strong></h1><br>
                                                <p class="text-danger font-size-20" >version 1.0.0</p>
                                                <!-- <h1 class="font-size-20 mt-4">Welcome, Please Login!</h1>
                                                <p class="text-muted">Login to continue to S.A.R.G.S.</p> -->

                                                
                                            </div>
                                            <hr>
                                            <div class="p-2 mt-5">
                                                <!-- <h1>Please Sign In</h1> -->
                                                <!-- <p>Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.</p> -->
                                                <!-- <br> -->
                                                <div id="login" class="form-horizontal">
                                                    <p class="text-danger text-center animate__animated animate__zoomIn" v-html="message.failed" v-if="message.failed"></p>

                                                    <div class="form-group auth-form-group-custom mb-4">
                                                        <!-- <i class="ri-user-2-line auti-custom-input-icon"></i> -->
                                                        <i class="fas fa-user auti-custom-input-icon"></i>
                                                        <label for="username" class="is-invalid">USERNAME</label>
                                                        <input type="text" class="form-control red-tooltip" :class="{'is-invalid': message.username}" placeholder="Please Enter Username" name="username"v-model="userLogin.username">
                                                    </div>
                                                    <p class="text-danger animate__animated animate__fadeIn" v-if="message.username" v-html="message.username"></p>
                            
                                                    <div class="form-group auth-form-group-custom mb-4">
                                                        <!-- <i class="ri-lock-2-line auti-custom-input-icon"></i> -->
                                                        <i class="fas fa-lock auti-custom-input-icon"></i>
                                                        <label for="userpassword">PASSWORD</label>
                                                        <input type="password" class="form-control" :class="{'is-invalid': message.username}" placeholder="Please Enter Password" name="password"v-model="userLogin.password" id="password">
                                                    </div>
                                                    <p class="text-danger animate__animated animate__fadeIn" v-if="message.password" v-html="message.password"></p>
                            
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="remember_me"  name="remember_me">
                                                        <label class="custom-control-label" for="remember_me">Remember me</label>
                                                    </div>

                                                    <div class="form-group mt-4 text-center">
                                                        <input type="submit" class="btn btn-block btn-sargs-button" value="Login" @click="login()">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                </div>

                 <!-- Copyright -->
                 <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                    2022 Developed by:
                    <a class="text-reset fw-bold" href="https://mdbootstrap.com/">&nbsp; Mr. Jose Mari C. Rey, MIT</a>
                </div>
                <!-- Copyright -->
        </main>



   
      

        <!-- JAVASCRIPT -->
        <script> 
            var url = "<?php echo base_url(); ?>"
        </script>
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vue.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vuex.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/axios.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/login.js"></script>
    </body>
</html>


