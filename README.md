# STUDENT ADMISSION REGISTRATION AND GRADING SYSTEM (SARGS)


###### ABOUT THE DEVELOPMENT
Author: Jose Mari Caballa Rey

###### CHANGE LOGS
| Date Commit  | Time Commit | Project Issues |
| :---: | :---: | :--- |
| 19 June 2022 | 1200 | making upload images |
| 15 June 2022 | 2345 | 2022 Developed by:   Mr. Jose Mari C. Rey, MIT |
| 28 April 2022 | 0105 | change login form design. |
| 11 April 2022 | 0124 | updating system development |
| 09 February 2022 | 2416 | updating user status |
| 07 February 2022 | 0144 | README.md update |
| 14 January 2022 | 1632 | README.md update |
| 14 January 2022 | 1616 | updated users module to make add, edit and delete user function |
| 13 January 2022 | 1505 | updated development |
| 10 January 2022 | 0052 | updated development |
| 19 December 2021 | 1205 | updated commit |
| 19 December 2021 | 1201 | updated commit |
| 29 July 2021 | 0228 | updated 07292021 0228; ranks reference module completed. |
| 04 June 2021 | 0800 | updated 06042021 full revision of system. |
| 07 April 2021 | 0101 | updated 04072021 0101; sibebar adjustment |
| 07 April 2021 | 2221 | updated 04062021 2221; changing font sizes for sidebar menu. |
| 06 April 2021 | 1014 | updated 04062021 1014pm; update codes. |
| 05 April 2021 | 2400 | revising changes |
| 20 March 2021 | 0800 | initial commit; first code igniter commit |

###### CONTACT
Facebook   : https://www.facebook.com/exceedcharge90<br/>
Mobile Nr  : 09298320301
